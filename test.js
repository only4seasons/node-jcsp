"use strict";

const jcsp = require(".");

const test1 = async () => {
  let server = new jcsp.Server();
  await server.listen(9870, "127.0.0.2");
  
  server.on("data", (data) => {
    console.info('TCP Server received data', data);
    server.close();
  });

  let client = new jcsp.Client();
  await client.connect(9870, "127.0.0.2");

  console.info("Client connected");
  client.send({
    apples: "this is an apple",
    oranges: [
      "these", "are", "oranges"
    ]
  });
}

test1();

const test2 = async () => {
  let server = new jcsp.Server();
  await server.listen("unix_socket");
  
  server.on("data", (data) => {
    console.info('UXS Server received data', data);
    server.close();
  });

  let client = new jcsp.Client();
  await client.connect("unix_socket");

  console.info("Client connected");
  client.send({
    apples: "this is another apple",
    oranges: [
      "these", "are", "more", "oranges"
    ]
  });
}

test2();