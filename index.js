"use strict";

const
  EventEmitter  = require("events").EventEmitter,
  net           = require("net");

function uniqid() {
  return Math.random().toString(36).substring(2);
}

class JsonStream extends EventEmitter {
  constructor() {
    super();
    let $this = this;
    let bf = "";
    let bf0 = null;
    let clen = 0; // current length
    let elen = 0; // expected length
    
    this.init = function() {
      bf = "";
      clen = 0;
      elen = 0;
    };
    
    this.write = function(data){
      if (bf0) {
        data = Buffer.concat(bf0, data);
        bf0 = null;
      }
      let offset = 0;
      do {
        if (elen-clen > 0) {
          let l   = Math.min(data.length-offset, elen - clen);
          clen   += l;
          bf     += data.toString('utf8', offset, l + offset);
          offset += l;
          
          if (clen == elen) {
            try {
              let result = JSON.parse(bf);
              $this.emit("data", result);
            }
            catch (e) {
              throw e;
            }
            finally {
              bf  = "";
            }
          }
        }
        else {
          if (data.length>=offset+4) {
            elen = data.readUInt32LE(offset);
            offset += 4;
            clen = 0;
          } else {
            bf0 = data.slice(offset);
            offset = data.length;
          }
        }
      } while (offset < data.length);
    }

  }
}

class Connection extends EventEmitter {
  constructor(socket) {
    super();
    let $this = this;
    let stream = new JsonStream();
    stream.on('data', function(data){
      $this.emit('data', data);
    });
    socket.on('data', stream.write);
    socket.on('close', function(){
      stream.init();
    })
    socket.on("error", (err) => {
      this.emit("error", err);
    });
    let lenbuf = Buffer.alloc(4);
    this.send = function(data) {
      let str = JSON.stringify(data);
      lenbuf.writeUInt32LE(str.length, 0);
      socket.write(lenbuf);
      socket.write(str);
    }
    this.close = () => {
      socket.destroy();
    }
  }
}

class Server extends EventEmitter {
  constructor() {
    super();
    
    this.clients = new Map();
    this.server = net.createServer();

    this.server.on("connection", (socket) => {
      let id = uniqid();
      while (this.clients.has(id)) {
        id = uniqid();
      }
      
      let connection = new Connection(socket);
      connection.id = id;
      this.clients.set(id, connection);
      
      connection.on("data", (data) => {
        this.emit("data", data, connection);
      });
      
      socket.on("close", () => {
        socket.removeAllListeners();
        this.clients.delete(id);
        this.emit("disconnect", connection);
      });
      
      this.emit("connect", connection);
    });

    process.on('SIGTERM', () => {
      this.server.close();
    });
    process.on("exit", () => {
      this.server.close();
    })
  }

  listen(...args) {
    return new Promise((resolve, reject) => {
      this.server.listen.call(this.server, ...args, () => {
        this.emit("ready");
        resolve();
      });
    });
  }

  broadcast(msg) {
    this.clients.forEach((client) => {
      client.send(msg);
    });
  }

  send(id, msg) {
    this.clients.has(id) && this.clients.get(id).send(msg);
  }

  close() {
    this.server.close();
    this.clients.forEach((client, key) => {
      client.close();
    });
  }

}

class Client extends EventEmitter {
  constructor() {
    super();
    this.connected = false;
    this.socket = new net.Socket();
    this.connection = new Connection(this.socket);
    
    this.connection.on("data", (data) => {
      this.emit("data", data);
    })
    
    this.socket.on("connect", () => {
      this.connected = true;
      this.emit('connect');
    });

    this.socket.on("error", (error) => {
      this.emit("error", error)
    });

    this.socket.on("close", () => {
      this.connected = false;
      this.emit("disconnect");
      //setTimeout(this.connect, 1000);
    });
  }

  connect(...args) {
    let $this = this;
    return new Promise((resolve, reject) => {
      try {
        return $this.socket.connect.call($this.socket, ...args, resolve);
      }
      catch (e) {
        reject(e);
      }
    });
  }

  send(data) {
    this.connected && this.connection.send(data);
  }

  close() {
    this.socket.close();
  }
}

module.exports = {
	Server: Server,
	Client: Client
};
